#ifndef STREAM9_XDG_TEST_MIME_DATA_DIR_HPP
#define STREAM9_XDG_TEST_MIME_DATA_DIR_HPP

#include "namespace.hpp"

#include <stream9/path/concat.hpp> // operator/
#include <stream9/string.hpp>

#include <boost/preprocessor/stringize.hpp>

namespace testing {

using st9::string;

inline string
data_dir()
{
    return string(BOOST_PP_STRINGIZE(DATA_DIR));
}

inline string
mime_data_dir()
{
    using stream9::path::operator/;

    return data_dir() / "mime";
}

} // namespace testing

#endif // STREAM9_XDG_TEST_MIME_DATA_DIR_HPP
