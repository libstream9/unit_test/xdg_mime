#include "data_directory.hpp"

#include <stream9/filesystem/mkdir_recursive.hpp>
#include <stream9/filesystem/remove_recursive.hpp>
#include <stream9/fstream.hpp>
#include <stream9/linux/mkdtemp.hpp>
#include <stream9/linux/unlink.hpp>
#include <stream9/path/concat.hpp> // operator/

#include <cstdlib>

namespace testing {

namespace path = stream9::path;
namespace lx { using namespace stream9::linux; }

using path::operator/;

static void
add_default_mime_types(auto& info)
{
    info.push_back({ "application/octet-stream", "binary data" });
    info.push_back({ "text/plain", "text data" });
    info.push_back({ "application/xml", "text xml" });
}

data_directory::
data_directory()
    : m_path { stream9::linux::mkdtemp("/tmp/") }
{
    add_default_mime_types(m_info);
}

data_directory::
data_directory(stream9::string_view path)
    : m_path { path }
{
    fs::mkdir_recursive(m_path);

    add_default_mime_types(m_info);
}

data_directory::
~data_directory() noexcept
{
    fs::remove_recursive(m_path);
}

void data_directory::
append(mime::xml::mime_type const& mt)
{
    m_info.push_back(mt);
}

void data_directory::
clear_mime_type()
{
    m_info = {};
    add_default_mime_types(m_info);
}

void data_directory::
create_mime_directory()
{
    auto path = m_path / "mime" / "packages";
    fs::mkdir_recursive(path);
}

void data_directory::
delete_mime_directory()
{
    auto path = m_path / "mime";
    fs::remove_recursive(path);
}

void data_directory::
update_database()
{
    create_mime_directory();

    auto mime_dir = m_path / "mime";
    auto package_path = mime_dir / "packages" / "test.xml";

    st9::ofstream ofs { package_path };
    ofs << m_info << std::flush;

    std::string cmdline { "update-mime-database " };
    cmdline.append(mime_dir);
    std::system(cmdline.c_str());
}

void data_directory::
delete_cache()
{
    using stream9::linux::unlink;

    auto path = m_path / "mime" / "mime.cache";

    unlink(path);
}

} // namespace testing
