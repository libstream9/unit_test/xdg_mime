#ifndef STREAM9_XDG_TEST_MIME_DATA_DIRECTORY_HPP
#define STREAM9_XDG_TEST_MIME_DATA_DIRECTORY_HPP

#include "namespace.hpp"

#include "src/xml/mime_type.hpp"
#include "src/xml/mime_info.hpp"

#include <stream9/string.hpp>

namespace testing {

class data_directory
{
public:
    data_directory();
    data_directory(stream9::string_view path);

    ~data_directory() noexcept;

    auto const& path() const { return m_path; }

    operator auto const& () const { return path(); }

    void append(mime::xml::mime_type const&);

    void clear_mime_type();

    void create_mime_directory();
    void delete_mime_directory();

    void update_database();

    void delete_cache();

private:
    stream9::string m_path;
    mime::xml::mime_info m_info;
};

} // namespace testing

#endif // STREAM9_XDG_TEST_MIME_DATA_DIRECTORY_HPP
