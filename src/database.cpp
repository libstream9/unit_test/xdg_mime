#include <stream9/xdg/mime/database.hpp>

#include "data_directory.hpp"
#include "make_paths.hpp"
#include "namespace.hpp"

#include "src/xml/mime_type.hpp"

#include <string_view>

#include <boost/test/unit_test.hpp>

#include <stream9/test/scoped_env.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(database_)

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::database const db;
    }

    BOOST_AUTO_TEST_CASE(glob_deleteall_)
    {
        data_directory dir1, dir2;

        auto const& path = make_paths(dir1.path(), dir2.path());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        {
            mime::xml::mime_type mt1 { "foo/bar", "comment" };
            mt1.append_glob("*.foo");
            mt1.append_glob("*.bar");
            dir1.append(mt1);
            dir1.update_database();

            mime::database db;

            auto const& mt = db.find_mime_type_for_filename("xxx.foo");
            BOOST_TEST(mt == "foo/bar");
        }

        {
            mime::xml::mime_type mt1 { "foo/bar", "comment" };
            mt1.append_glob("*.bar");
            mt1.set_glob_deleteall();

            dir2.append(mt1);
            dir2.update_database();

            mime::database db;

            auto const& mt = db.find_mime_type_for_filename("xxx.foo");
            BOOST_TEST(mt == "application/octet-stream");
        }
    }

    BOOST_AUTO_TEST_CASE(magic_deleteall_)
    {
        data_directory dir1, dir2;

        auto const& path = make_paths(dir1.path(), dir2.path());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        {
            mime::xml::mime_type mt1 { "foo/bar", "comment" };
            mt1.append_magic(mime::xml::mime_type::magic { 50, {
                    mime::xml::mime_type::match { "string", "0", "xyz", "", {}, },
                }
            });
            dir1.append(mt1);
            dir1.update_database();

            mime::database db;

            std::string_view data { "xyz123456" };
            auto const& mt = db.find_mime_type_for_data(data);
            BOOST_TEST(mt == "foo/bar");
        }

        {
            mime::xml::mime_type mt1 { "foo/bar", "comment" };
            mt1.set_magic_deleteall();
            dir2.append(mt1);
            dir2.update_database();

            mime::database db;

            std::string_view data { "xyz123456" };
            auto const& mt = db.find_mime_type_for_data(data);
            BOOST_TEST(mt == "text/plain");
        }
    }

BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
