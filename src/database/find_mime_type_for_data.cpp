#include <stream9/xdg/mime/database.hpp>

#include "../data_directory.hpp"
#include "../make_paths.hpp"
#include "../namespace.hpp"

#include "src/xml/mime_type.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/test/scoped_env.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(database_)
BOOST_AUTO_TEST_SUITE(find_mime_type_for_data_)

    BOOST_AUTO_TEST_CASE(no_match_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        dir1.update_database();

        mime::database db;

        std::string_view data { "foo" };
        auto const& mt = db.find_mime_type_for_data(data);
        BOOST_TEST(mt == "text/plain");
    }

    BOOST_AUTO_TEST_CASE(single_match_)
    {
        data_directory dir1;

        using mime::xml::mime_type;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime::xml::mime_type mt1 { "foo/bar", "comment" };
            mt1.append_magic(mime_type::magic { 50, {
                    mime_type::match { "string", "0", "foo", "", {}, },
                }
            });
            dir1.append(mt1);
            dir1.update_database();
        }

        mime::database db;

        std::string_view data { "foobar" };
        auto const& mt = db.find_mime_type_for_data(data);
        BOOST_TEST(mt == "foo/bar");
    }

    BOOST_AUTO_TEST_CASE(multiple_match_)
    {
        data_directory dir1;

        using mime::xml::mime_type;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime::xml::mime_type mt1 { "foo/bar", "comment" };
            mt1.append_magic(mime_type::magic { 40, {
                    mime_type::match { "string", "0", "foo", "", {}, },
                }
            });
            dir1.append(mt1);
        }

        {
            mime::xml::mime_type mt2 { "foo/baz", "comment" };
            mt2.append_magic(mime_type::magic { 60, {
                    mime_type::match { "string", "3", "bar", "", {}, },
                }
            });
            dir1.append(mt2);
        }

        dir1.update_database();

        mime::database db;

        std::string_view data { "foobar" };
        auto const& mt = db.find_mime_type_for_data(data);
        BOOST_TEST(mt == "foo/baz");
    }

BOOST_AUTO_TEST_SUITE_END() // find_mime_type_for_data_
BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
