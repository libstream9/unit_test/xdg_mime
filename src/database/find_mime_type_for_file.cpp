#include <stream9/xdg/mime/database.hpp>

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/temporary_file.hpp>
#include <stream9/test/scoped_env.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(database_)
BOOST_AUTO_TEST_SUITE(find_mime_type_for_file_)

    BOOST_AUTO_TEST_CASE(by_filename_only_)
    {
        test::scoped_env env { "XDG_DATA_DIRS", data_dir() };
        mime::database const db;

        auto m = db.find_mime_type_for_file("Makefile");

        BOOST_TEST(m.name() == "text/x-makefile");
    }

    BOOST_AUTO_TEST_CASE(by_filename_and_its_content_)
    {
        test::scoped_env env { "XDG_DATA_DIRS", data_dir() };
        mime::database const db;

        fs::temporary_file tmp;

        tmp << "#!/bin/bash" << std::flush;

        auto m = db.find_mime_type_for_file(tmp.path().c_str());

        BOOST_TEST(m.name() == "application/x-shellscript");
    }

    BOOST_AUTO_TEST_CASE(ambiguous_name_)
    {
        test::scoped_env env { "XDG_DATA_DIRS", data_dir() };
        mime::database const db;

        auto m = db.find_mime_type_for_file("foo.pdf.gz");

        BOOST_TEST(m.name() == "application/x-gzpdf");
    }

BOOST_AUTO_TEST_SUITE_END() // find_mime_type_for_file_
BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
