#include <stream9/xdg/mime/database.hpp>

#include "../data_directory.hpp"
#include "../make_paths.hpp"
#include "../namespace.hpp"

#include "src/xml/mime_type.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/test/scoped_env.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(database_)
BOOST_AUTO_TEST_SUITE(find_mime_type_for_name_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path().c_str());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        mime::xml::mime_type mt1 { "foo/bar", "comment" };
        dir1.append(mt1);

        dir1.update_database();

        mime::database db;

        auto o_m = db.find_mime_type_for_name("foo/bar");
        BOOST_TEST(o_m.has_value());
    }

    BOOST_AUTO_TEST_CASE(fail_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        mime::xml::mime_type mt1 { "foo/baz", "comment" };
        dir1.append(mt1);

        dir1.update_database();

        mime::database db;

        auto o_m = db.find_mime_type_for_name("foo/bar");
        BOOST_TEST(!o_m);
    }

BOOST_AUTO_TEST_SUITE_END() // find_mime_type_for_name_
BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
