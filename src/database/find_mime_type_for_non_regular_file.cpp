#include <stream9/xdg/mime/database.hpp>

#include "../data_directory.hpp"
#include "../make_paths.hpp"
#include "../namespace.hpp"

#include "src/xml/mime_type.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/filesystem/tmpfstream.hpp>
#include <stream9/linux/symlink.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/test/scoped_env.hpp>

namespace testing {

using stream9::path::operator/;

namespace lx = stream9::linux;

BOOST_AUTO_TEST_SUITE(database_)
BOOST_AUTO_TEST_SUITE(find_mime_type_for_non_regular_file_)

    BOOST_AUTO_TEST_CASE(no_match_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        dir1.update_database();

        mime::database db;

        fs::tmpfstream f1;
        f1 << "foobar" << std::flush;

        auto const& mt_set = db.find_mime_type_for_non_regular_file(f1.path().c_str());

        BOOST_TEST(mt_set.empty());
    }

    BOOST_AUTO_TEST_CASE(directory_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime::xml::mime_type mt { "inode/directory", "directory" };
            dir1.append(mt);

            dir1.update_database();
        }

        mime::database db;

        fs::temporary_directory d1;

        auto const& mt_set = db.find_mime_type_for_non_regular_file(d1.path().c_str());
        auto const expected = { "inode/directory" };

        BOOST_TEST(mt_set == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(mount_point_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            dir1.append({ "inode/directory", "directory" });
            dir1.append({ "inode/mount-point", "mount point" });

            dir1.update_database();
        }

        mime::database db;

        auto const& mt_set = db.find_mime_type_for_non_regular_file("/");
        auto const expected = { "inode/directory", "inode/mount-point" };

        BOOST_TEST(mt_set == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(symlink_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            dir1.append({ "inode/directory", "directory" });
            dir1.append({ "inode/mount-point", "mount point" });
            dir1.append({ "inode/symlink", "symbolic link" });

            dir1.update_database();
        }

        mime::database db;

        fs::temporary_directory tmp_dir;
        auto const& symlink_path = tmp_dir.path() / "root";
        lx::symlink("/", symlink_path);

        auto const& mt_set = db.find_mime_type_for_non_regular_file(symlink_path);
        auto const expected = { "inode/directory", "inode/symlink" };

        BOOST_TEST(mt_set == expected, boost::test_tools::per_element());
    }

BOOST_AUTO_TEST_SUITE_END() // find_mime_type_for_non_regular_file_
BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
