#include <stream9/xdg/mime/database.hpp>
#include <stream9/xdg/mime/error.hpp>

#include "../data_directory.hpp"
#include "../make_paths.hpp"
#include "../namespace.hpp"

#include "src/xml/mime_type.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/test/scoped_env.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(database_)
BOOST_AUTO_TEST_SUITE(find_mime_type_for_xml_)

    BOOST_AUTO_TEST_CASE(no_match_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        dir1.update_database();

        mime::database db;

        auto const xml = "<foobar xmlns=\"http://foo.bar\"/>";

        auto mt_set = db.find_mime_type_for_xml(xml);

        auto const expected = { "application/xml" };

        BOOST_TEST(mt_set == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(single_match_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime::xml::mime_type mt { "foo/bar", "comment" };
            mt.append_root_xml("http://foo.bar", "foobar");

            dir1.append(mt);

            dir1.update_database();
        }

        mime::database db;

        auto const xml = "<foobar xmlns=\"http://foo.bar\"/>";

        auto mt_set = db.find_mime_type_for_xml(xml);

        auto const expected = { "foo/bar" };

        BOOST_TEST(mt_set == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(multiple_match_)
    {
        data_directory dir1, dir2;

        auto const& path = make_paths(dir1.path(), dir2.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime::xml::mime_type mt { "foo/bar", "comment" };
            mt.append_root_xml("http://foo.bar", "foobar");

            dir1.append(mt);

            dir1.update_database();
        }
        {
            mime::xml::mime_type mt { "foo/baz", "comment" };
            mt.append_root_xml("http://foo.bar", "foobar");

            dir2.append(mt);

            dir2.update_database();
        }

        mime::database db;

        auto const xml = "<foobar xmlns=\"http://foo.bar\"/>";

        auto mt_set = db.find_mime_type_for_xml(xml);

        auto const expected = { "foo/bar", "foo/baz" };

        BOOST_TEST(mt_set == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(multiple_match_with_duplication_)
    {
        data_directory dir1, dir2;

        auto const& path = make_paths(dir1.path(), dir2.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime::xml::mime_type mt { "foo/bar", "comment" };
            mt.append_root_xml("http://foo.bar", "foobar");

            dir1.append(mt);

            dir1.update_database();
        }
        {
            mime::xml::mime_type mt { "foo/bar", "comment" };
            mt.append_root_xml("http://foo.bar", "foobar");

            dir2.append(mt);

            dir2.update_database();
        }

        mime::database db;

        auto const xml = "<foobar xmlns=\"http://foo.bar\"/>";

        auto mt_set = db.find_mime_type_for_xml(xml);

        auto const expected = { "foo/bar" };

        BOOST_TEST(mt_set == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(invalid_xml_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime::xml::mime_type mt { "foo/bar", "comment" };
            mt.append_root_xml("http://foo.bar", "foobar");

            dir1.append(mt);

            dir1.update_database();
        }

        mime::database db;

        auto const xml = "<foobar xmlns=\"http://foo.bar\">";

        BOOST_CHECK_THROW(db.find_mime_type_for_xml(xml), stream9::error);
    }

BOOST_AUTO_TEST_SUITE_END() // find_mime_type_for_xml_
BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
