#include <stream9/xdg/mime/database.hpp>

#include "../data_directory.hpp"
#include "../make_paths.hpp"
#include "../namespace.hpp"

#include "src/xml/mime_type.hpp"

#include <boost/test/unit_test.hpp>

#include <ranges>

#include <stream9/ranges/at.hpp>
#include <stream9/test/scoped_env.hpp>

namespace testing {

namespace rng {

auto&&
at(auto&& rng, auto i)
{
    return rng[i];
}

} // namespace rng

BOOST_AUTO_TEST_SUITE(database_)
BOOST_AUTO_TEST_SUITE(find_mime_types_for_data_)

    BOOST_AUTO_TEST_CASE(no_match_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        dir1.update_database();

        mime::database db;

        std::string_view data { "\x01\x02\x03\x04" };
        auto const& types = db.find_mime_types_for_data(data);
        BOOST_REQUIRE(types.size() == 1);
        BOOST_TEST(rng::at(types, 0) == "application/octet-stream");
    }

    BOOST_AUTO_TEST_CASE(single_match_)
    {
        data_directory dir1;

        using mime::xml::mime_type;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        mime::xml::mime_type mt1 { "foo/bar", "comment" };
        mt1.append_magic(mime_type::magic { 50, {
                mime_type::match { "byte", "0", "0x01", "", {}, },
            }
        });
        dir1.append(mt1);
        dir1.update_database();

        mime::database db;

        std::string_view data { "\x01\x02\x03\x04" };
        auto const& types = db.find_mime_types_for_data(data);
        BOOST_REQUIRE(types.size() == 1);
        BOOST_TEST(rng::at(types, 0) == "foo/bar");
    }

    BOOST_AUTO_TEST_CASE(multiple_match_)
    {
        data_directory dir1;

        using mime::xml::mime_type;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        mime::xml::mime_type mt1 { "foo/bar", "comment" };
        mt1.append_magic(mime_type::magic { 50, {
                mime_type::match { "byte", "0", "0x01", "", {}, },
            }
        });
        dir1.append(mt1);

        mime::xml::mime_type mt2 { "foo/baz", "comment" };
        mt2.append_magic(mime_type::magic { 50, {
                mime_type::match { "byte", "3", "0x04", "", {}, },
            }
        });
        dir1.append(mt2);

        dir1.update_database();

        mime::database db;

        std::string_view data { "\x01\x02\x03\x04" };
        auto types = db.find_mime_types_for_data(data);
        std::ranges::sort(types);

        auto const expected = {
            "foo/bar",
            "foo/baz",
        };
        BOOST_TEST(types == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(multiple_match_with_duplication_)
    {
        data_directory dir1, dir2;

        using mime::xml::mime_type;

        auto const& path = make_paths(dir1.path(), dir2.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime::xml::mime_type mt1 { "foo/bar", "comment" };
            mt1.append_magic(mime_type::magic { 50, {
                    mime_type::match { "byte", "0", "0x01", "", {}, },
                }
            });
            dir1.append(mt1);
            dir1.update_database();
        }

        {
            mime::xml::mime_type mt2 { "foo/bar", "comment" };
            mt2.append_magic(mime_type::magic { 50, {
                    mime_type::match { "byte", "3", "0x04", "", {}, },
                }
            });
            dir2.append(mt2);
            dir2.update_database();
        }

        mime::database db;

        std::string_view data { "\x01\x02\x03\x04" };
        auto types = db.find_mime_types_for_data(data);
        std::ranges::sort(types);
        BOOST_REQUIRE_EQUAL(types.size(), 1);
        BOOST_TEST(rng::at(types, 0) == "foo/bar");
    }

BOOST_AUTO_TEST_SUITE_END() // find_mime_types_for_data_
BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
