#include <stream9/xdg/mime/database.hpp>

#include "../namespace.hpp"
#include "../data_directory.hpp"
#include "../make_paths.hpp"

#include "src/xml/mime_type.hpp"

#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/ranges/at.hpp>
#include <stream9/test/scoped_env.hpp>

namespace testing {

namespace rng {

auto&&
at(auto&& rng, auto i)
{
    return rng[i];
}

} // namespace rng

BOOST_AUTO_TEST_SUITE(database_)
BOOST_AUTO_TEST_SUITE(find_mime_types_for_filename_)

    BOOST_AUTO_TEST_CASE(no_match_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path().c_str());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        dir1.update_database();

        mime::database db;

        auto const& types = db.find_mime_types_for_filename("xxx.foo");
        BOOST_REQUIRE(types.size() == 1);
        BOOST_TEST(rng::at(types, 0) == "application/octet-stream");
    }

    BOOST_AUTO_TEST_CASE(single_match_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path().c_str());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        mime::xml::mime_type mt1 { "foo/bar", "comment" };
        mt1.append_glob("*.foo");
        dir1.append(mt1);
        dir1.update_database();

        mime::database db;

        auto const& types = db.find_mime_types_for_filename("xxx.foo");
        BOOST_REQUIRE(types.size() == 1);
        BOOST_TEST(rng::at(types, 0) == "foo/bar");
    }

    BOOST_AUTO_TEST_CASE(multiple_match_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        mime::xml::mime_type mt1 { "test/type1", "comment" };
        mt1.append_glob("*.x.y", 50);
        dir1.append(mt1);

        mime::xml::mime_type mt2 { "test/type2", "comment" };
        mt2.append_glob("*.y", 60);
        dir1.append(mt2);

        dir1.update_database();

        mime::database db;

        auto candidates = db.find_mime_types_for_filename("foo.x.y");
        std::ranges::sort(candidates);
        BOOST_REQUIRE(candidates.size() == 2);
        BOOST_TEST(rng::at(candidates, 0) == "test/type1");
        BOOST_TEST(rng::at(candidates, 1) == "test/type2");
    }

    BOOST_AUTO_TEST_CASE(multiple_match_with_duplication_)
    {
        data_directory dir1, dir2;

        auto const& path1 = make_paths(dir1.path(), dir2.path());
        test::scoped_env env { "XDG_DATA_DIRS", path1 };

        mime::xml::mime_type mt1 { "test/type1", "comment" };
        mt1.append_glob("*.x");
        dir1.append(mt1);

        dir1.update_database();

        mime::xml::mime_type mt2 { "test/type1", "comment" };
        mt2.append_glob("*.x");
        dir2.append(mt2);

        mime::xml::mime_type mt3 { "test/type2", "comment" };
        mt3.append_glob("*.x");
        dir2.append(mt3);

        dir2.update_database();

        mime::database db;

        auto candidates = db.find_mime_types_for_filename("foo.x");
        std::ranges::sort(candidates);
        BOOST_REQUIRE(candidates.size() == 2);
        BOOST_TEST(rng::at(candidates, 0) == "test/type1");
        BOOST_TEST(rng::at(candidates, 1) == "test/type2");
    }

BOOST_AUTO_TEST_SUITE_END() // find_mime_types_for_filename_
BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
