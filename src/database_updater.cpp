#include <stream9/xdg/mime/database_updater.hpp>

#include "data_directory.hpp"
#include "make_paths.hpp"
#include "namespace.hpp"

#include "src/xml/mime_type.hpp"

#include <string_view>

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/test/scoped_env.hpp>
#include <stream9/log.hpp>

namespace testing {

using stream9::path::operator/;

namespace log = stream9::log;

BOOST_AUTO_TEST_SUITE(database_updater_)

    BOOST_AUTO_TEST_CASE(mime_info_added_)
    {
        data_directory dir1;
        log::set_level(log::priority::warning);

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        {
            mime::xml::mime_type mt1 { "foo/foo", "comment" };
            mt1.append_glob("*.foo");
            dir1.append(mt1);
            dir1.update_database();
        }

        mime::database db;

        auto const& mt1 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt1 == "foo/foo");

        auto const& mt2 = db.find_mime_type_for_filename("xxx.bar");
        BOOST_TEST(mt2 == "application/octet-stream");

        mime::database_updater updater { db };
        {
            mime::xml::mime_type mt1 { "bar/baz", "comment" };
            mt1.append_glob("*.baz");
            dir1.append(mt1);
            dir1.update_database();
        }
        updater.process_event();

        auto const& mt3 = db.find_mime_type_for_filename("xxx.baz");
        BOOST_TEST(mt3 == "bar/baz");
    }

    BOOST_AUTO_TEST_CASE(mime_info_removed_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        {
            mime::xml::mime_type mt1 { "foo/foo", "comment" };
            mt1.append_glob("*.foo");
            dir1.append(mt1);

            mime::xml::mime_type mt2 { "bar/bar", "comment" };
            mt2.append_glob("*.bar");
            dir1.append(mt2);

            dir1.update_database();
        }

        mime::database db;

        auto const& mt1 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt1 == "foo/foo");

        auto const& mt2 = db.find_mime_type_for_filename("xxx.bar");
        BOOST_TEST(mt2 == "bar/bar");

        mime::database_updater updater { db };
        {
            dir1.clear_mime_type();

            mime::xml::mime_type mt1 { "bar/bar", "comment" };
            mt1.append_glob("*.bar");
            dir1.append(mt1);

            dir1.update_database();
        }
        updater.process_event();

        auto const& mt3 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt3 == "application/octet-stream");
    }

    BOOST_AUTO_TEST_CASE(cache_created_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        dir1.create_mime_directory();

        mime::xml::mime_type mt1 { "foo/foo", "comment" };
        mt1.append_glob("*.foo");
        dir1.append(mt1);

        mime::database db;
        mime::database_updater updater { db };

        auto const& mt2 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt2 == "application/octet-stream");

        dir1.update_database();
        updater.process_event();

        auto const& mt3 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt3 == "foo/foo");
    }

    BOOST_AUTO_TEST_CASE(cache_deleted_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        dir1.create_mime_directory();

        mime::xml::mime_type mt1 { "foo/foo", "comment" };
        mt1.append_glob("*.foo");
        dir1.append(mt1);

        dir1.update_database();

        mime::database db;
        mime::database_updater updater { db };

        auto const& mt2 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt2 == "foo/foo");

        dir1.delete_cache();

        updater.process_event();

        auto const& mt3 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt3 == "application/octet-stream");
    }

    BOOST_AUTO_TEST_CASE(mime_directory_created_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        mime::database db;
        mime::database_updater updater { db };

        auto const& mt2 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt2 == "application/octet-stream");

        mime::xml::mime_type mt1 { "foo/foo", "comment" };
        mt1.append_glob("*.foo");
        dir1.append(mt1);

        dir1.update_database();

        updater.process_event();

        auto const& mt3 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt3 == "foo/foo");
    }

    BOOST_AUTO_TEST_CASE(mime_directory_deleted_)
    {
        data_directory dir1;

        auto path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path.c_str() };

        mime::xml::mime_type mt1 { "foo/foo", "comment" };
        mt1.append_glob("*.foo");
        dir1.append(mt1);

        dir1.update_database();

        mime::database db;
        mime::database_updater updater { db };

        auto mt2 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt2 == "foo/foo");

        dir1.delete_mime_directory();
        updater.process_event();

        auto const& mt3 = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt3 == "application/octet-stream");
    }

BOOST_AUTO_TEST_SUITE_END() // database_updater_

} // namespace testing
