#include <stream9/xdg/mime/environment.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using stream9::xdg::mime::environment;

BOOST_AUTO_TEST_SUITE(environment_)

    BOOST_AUTO_TEST_CASE(real_path_icase_)
    {
        environment env;

        BOOST_TEST(env.real_path_icase("/") == "/");
        BOOST_TEST(env.real_path_icase("/USR") == "/usr");
        BOOST_TEST(env.real_path_icase("/USR/Bin") == "/usr/bin");
        BOOST_CHECK(env.real_path_icase("/USR/Bim") == stream9::null);
        BOOST_TEST(env.real_path_icase("/USR/Bin/../../../eTc") == "/usr/bin/../../../etc");
        BOOST_TEST(env.real_path_icase("../../../../../../../../Home/../Usr/bin") == "../../../../../../../../home/../usr/bin");
    }

BOOST_AUTO_TEST_SUITE_END() // environment_

} // namespace testing
