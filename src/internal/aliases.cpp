#include "src/aliases.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include "src/cache.hpp"

#include <ranges>
#include <iterator>

#include <boost/test/unit_test.hpp>

#include <stream9/ranges/back.hpp>
#include <stream9/ranges/front.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(alias_entry_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::alias_entry;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto s = rng::front(c.aliases());

        auto [alias, mime_type] = s;

        static_assert(std::same_as<decltype(alias), st9::cstring_view>);
        static_assert(std::same_as<decltype(mime_type), st9::cstring_view>);
    }

BOOST_AUTO_TEST_SUITE_END() // alias_entry_

BOOST_AUTO_TEST_SUITE(aliases_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::aliases;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
        static_assert(std::ranges::random_access_range<T>);
        static_assert(std::ranges::sized_range<T>);
        static_assert(std::ranges::borrowed_range<T>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& s = c.aliases();
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.aliases();

        auto it = s.begin();
        auto end = s.end();

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);

        BOOST_TEST((end - it) == 289);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.aliases();

        auto [it, end] = s;

        BOOST_TEST((end - it) == 289);
    }

    BOOST_AUTO_TEST_CASE(borrowed_range_)
    {
        mime::cache c { mime_data_dir() };

        auto it = c.aliases().begin();

        BOOST_TEST((*it).alias() == "application/acrobat");
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.aliases();

        BOOST_TEST(s.size() == 289);
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.aliases();

        auto const& ent = rng::front(s);

        BOOST_TEST(ent.alias() == "application/acrobat");
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.aliases();

        auto const& ent = rng::back(s);

        BOOST_TEST(ent.alias() == "zz-application/zz-winassoc-xls");
    }

    BOOST_AUTO_TEST_CASE(middle_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.aliases();

        auto const& ent = s[s.size() / 2];

        BOOST_TEST(ent.alias() == "audio/mpegurl");
    }

    BOOST_AUTO_TEST_CASE(resolve_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.aliases();

        auto const& m = s.search("audio/mpegurl");
        BOOST_REQUIRE(m);
        BOOST_TEST(*m == "audio/x-mpegurl");
    }

BOOST_AUTO_TEST_SUITE_END() // aliases_

} // namespace testing
