#include "src/cache.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include "src/glob_record.hpp"
#include "src/directory.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/array.hpp>

namespace testing {

using stream9::array;

BOOST_AUTO_TEST_SUITE(cache_)

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
    }

    BOOST_AUTO_TEST_CASE(version_)
    {
        mime::cache c { mime_data_dir() };

        BOOST_TEST(c.major_version() == 1);
        BOOST_TEST(c.minor_version() == 2);
    }

    BOOST_AUTO_TEST_SUITE(glob_search_)

        BOOST_AUTO_TEST_CASE(literal_)
        {
            mime::cache c { mime_data_dir() };

            array<mime::glob_record> matches;
            c.glob_search(matches, "core");

            BOOST_REQUIRE(matches.size() == 1);
            BOOST_TEST(matches[0].pattern == "core");
            BOOST_TEST(matches[0].mime_type == "application/x-core");
            BOOST_CHECK(matches[0].flags == mime::glob_flag::case_sensitive);
            BOOST_TEST(matches[0].weight == 50);
        }

        BOOST_AUTO_TEST_CASE(suffix_match_)
        {
            mime::cache c { mime_data_dir() };

            array<mime::glob_record> matches;
            c.glob_search(matches, "test.C");

            BOOST_REQUIRE(matches.size() == 1);
            BOOST_TEST(matches[0].pattern == "*.C");
            BOOST_TEST(matches[0].mime_type == "text/x-c++src");
            BOOST_CHECK(matches[0].flags == mime::glob_flag::case_sensitive);
            BOOST_TEST(matches[0].weight == 50);
        }

        BOOST_AUTO_TEST_CASE(fnmatch_)
        {
            mime::cache c { mime_data_dir() };

            array<mime::glob_record> matches;
            c.glob_search(matches, "Readme.Txt");

            BOOST_REQUIRE(matches.size() == 2);
            BOOST_TEST(matches[0].pattern == "*.txt");
            BOOST_TEST(matches[0].mime_type == "text/plain");
            BOOST_CHECK(matches[0].flags == mime::glob_flag::none);
            BOOST_TEST(matches[0].weight == 50);
            BOOST_TEST(matches[1].pattern == "readme*");
            BOOST_TEST(matches[1].mime_type == "text/x-readme");
            BOOST_CHECK(matches[1].flags == mime::glob_flag::none);
            BOOST_TEST(matches[1].weight == 10);
        }

    BOOST_AUTO_TEST_SUITE_END() // glob_search_

BOOST_AUTO_TEST_SUITE_END() // cache_

} // namespace testing
