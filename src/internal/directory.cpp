#include "src/directory.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(directory_)

    BOOST_AUTO_TEST_CASE(construct_)
    {
        auto const path = mime_data_dir();
        mime::directory dir { path };

        BOOST_TEST(dir.path() == path);
    }

BOOST_AUTO_TEST_SUITE_END() // directory_

} // namespace testing
