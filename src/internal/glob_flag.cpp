#include "src/glob_flag.hpp"

#include "../namespace.hpp"

#include <concepts>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(glob_flag_)

    using mime::glob_flag;
    using mime::glob_flags;

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        static_assert(std::regular<glob_flag>);
        static_assert(std::regular<glob_flags>);
    }

    BOOST_AUTO_TEST_CASE(default_initialize_)
    {
        glob_flags f;

        BOOST_CHECK(f == glob_flag());
        BOOST_CHECK(f == glob_flag::none);
    }

    BOOST_AUTO_TEST_CASE(value_initialize_)
    {
        glob_flags f { glob_flag::case_sensitive };

        BOOST_CHECK(f == glob_flag::case_sensitive);
    }

    BOOST_AUTO_TEST_CASE(bool_context_)
    {
        glob_flags f1;

        BOOST_CHECK(!f1);

        glob_flags f2 = glob_flag::case_sensitive;
        BOOST_CHECK(f2);
    }

    BOOST_AUTO_TEST_CASE(bit_or_1_)
    {
        glob_flags f;

        f |= glob_flag::case_sensitive;

        BOOST_CHECK(f == glob_flag::case_sensitive);
    }

    BOOST_AUTO_TEST_CASE(bit_or_2_)
    {
        glob_flags f1;

        auto const f2 = f1 | glob_flag::case_sensitive;

        BOOST_CHECK(f2 == glob_flag::case_sensitive);
    }

    BOOST_AUTO_TEST_CASE(bit_and_1_)
    {
        glob_flags f { glob_flag::case_sensitive };

        f &= glob_flag::none;

        BOOST_CHECK(f == glob_flag::none);
    }

    BOOST_AUTO_TEST_CASE(bit_and_2_)
    {
        glob_flags f1 { glob_flag::case_sensitive };

        auto const f2 = f1 & glob_flag::none;

        BOOST_CHECK(f2 == glob_flag::none);
    }

    BOOST_AUTO_TEST_CASE(bit_not_1_)
    {
        glob_flags f { glob_flag::case_sensitive };

        f ^= glob_flag::case_sensitive;

        BOOST_CHECK(f == glob_flag::none);
    }

    BOOST_AUTO_TEST_CASE(bit_not_2_)
    {
        glob_flags f1 { glob_flag::case_sensitive };

        auto const f2 = f1 ^ glob_flag::case_sensitive;

        BOOST_CHECK(f2 == glob_flag::none);
    }

BOOST_AUTO_TEST_SUITE_END() // glob_flag_

} // namespace testing
