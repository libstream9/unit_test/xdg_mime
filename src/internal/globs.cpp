#include "src/globs.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include "src/cache.hpp"

#include <concepts>
#include <iterator>
#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/ranges/back.hpp>
#include <stream9/ranges/front.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(glob_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::glob;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& ent = rng::front(c.globs());
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& ent = rng::front(c.globs());

        auto [pattern, mime_type, flag, weight] = ent;

        static_assert(std::same_as<decltype(pattern), st9::cstring_view>);
        static_assert(std::same_as<decltype(mime_type), st9::cstring_view>);
        static_assert(std::same_as<decltype(flag), uint32_t>);
        static_assert(std::same_as<decltype(weight), uint32_t>);

        BOOST_TEST(pattern == "*.anim[1-9j]");
        BOOST_TEST(mime_type == "video/x-anim");
        BOOST_TEST(flag == 0);
        BOOST_TEST(weight == 50);
    }

BOOST_AUTO_TEST_SUITE_END() // glob_

BOOST_AUTO_TEST_SUITE(globs_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::globs;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
        static_assert(std::ranges::random_access_range<T>);
        static_assert(std::ranges::sized_range<T>);
        static_assert(std::ranges::borrowed_range<T>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& g = c.globs();
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.globs();

        auto it = g.begin();
        auto end = g.end();

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);

        BOOST_TEST((end - it) == 10);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.globs();

        auto [it, end] = g;

        BOOST_TEST((end - it) == 10);
    }

    BOOST_AUTO_TEST_CASE(borrowed_range_)
    {
        mime::cache c { mime_data_dir() };

        auto it = c.globs().begin();

        BOOST_TEST((*it).pattern() == "*.anim[1-9j]");
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.globs();

        BOOST_TEST(g.size() == 10);
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.globs();

        auto const& ent = rng::front(g);

        BOOST_TEST(ent.pattern() == "*.anim[1-9j]");
        BOOST_TEST(ent.mime_type() == "video/x-anim");
        BOOST_TEST(ent.flags() == 0);
        BOOST_TEST(ent.weight() == 50);
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.globs();

        auto const& ent = rng::back(g);

        BOOST_TEST(ent.pattern() == "sconscript.*");
        BOOST_TEST(ent.mime_type() == "text/x-scons");
        BOOST_TEST(ent.flags() == 0);
        BOOST_TEST(ent.weight() == 50);
    }

    BOOST_AUTO_TEST_CASE(middle_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.globs();

        auto const& ent = g[g.size() / 2];

        BOOST_TEST(ent.pattern() == "callgrind.out*");
        BOOST_TEST(ent.mime_type() == "application/x-kcachegrind");
        BOOST_TEST(ent.flags() == 0);
        BOOST_TEST(ent.weight() == 50);
    }

BOOST_AUTO_TEST_SUITE_END() // globs_

} // namespace testing
