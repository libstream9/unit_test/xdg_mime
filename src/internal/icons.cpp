#include "src/icons.hpp"

#include "../namespace.hpp"
#include "../data_dir.hpp"

#include "src/cache.hpp"

#include <concepts>
#include <iterator>
#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/ranges/back.hpp>
#include <stream9/ranges/front.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(icon_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::icon;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& ent = rng::front(c.icons());
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& ent = rng::front(c.icons());

        auto [mime_type, name] = ent;

        static_assert(std::same_as<decltype(mime_type), st9::cstring_view>);
        static_assert(std::same_as<decltype(name), st9::cstring_view>);

        BOOST_TEST(mime_type == "application/x-keepass2");
        BOOST_TEST(name == "application-x-keepassx");
    }

BOOST_AUTO_TEST_SUITE_END() // icon_

BOOST_AUTO_TEST_SUITE(icons_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::icons;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
        static_assert(std::ranges::random_access_range<T>);
        static_assert(std::ranges::sized_range<T>);
        static_assert(std::ranges::borrowed_range<T>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& g = c.icons();
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.icons();

        auto it = g.begin();
        auto end = g.end();

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);

        BOOST_TEST((end - it) == 9);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.icons();

        auto [it, end] = g;

        BOOST_TEST((end - it) == 9);
    }

    BOOST_AUTO_TEST_CASE(borrowed_range_)
    {
        mime::cache c { mime_data_dir() };

        auto it = c.icons().begin();

        BOOST_TEST((*it).mime_type() == "application/x-keepass2");
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.icons();

        BOOST_TEST(g.size() == 9);
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.icons();

        auto const& ent = rng::front(g);

        BOOST_TEST(ent.mime_type() == "application/x-keepass2");
        BOOST_TEST(ent.name() == "application-x-keepassx");
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.icons();

        auto const& ent = rng::back(g);

        BOOST_TEST(ent.mime_type() == "application/x-virtualbox-vmdk");
        BOOST_TEST(ent.name() == "virtualbox-vmdk");
    }

    BOOST_AUTO_TEST_CASE(middle_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.icons();

        auto const& ent = g[g.size() / 2];

        BOOST_TEST(ent.mime_type() == "application/x-virtualbox-vbox");
        BOOST_TEST(ent.name() == "virtualbox-vbox");
    }

    BOOST_AUTO_TEST_CASE(search_)
    {
        mime::cache c { mime_data_dir() };

        auto o_icon = c.icons().search("text/plain");
        BOOST_TEST(!o_icon);

        o_icon = c.icons().search("application/x-keepass2");
        BOOST_REQUIRE(o_icon);
        BOOST_TEST(o_icon->name() == "application-x-keepassx");
    }

BOOST_AUTO_TEST_SUITE_END() // icons_

} // namespace testing
