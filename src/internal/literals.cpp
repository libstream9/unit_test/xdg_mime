#include "src/globs.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include "src/cache.hpp"

#include <ranges>
#include <iterator>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(literals_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::globs;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
        static_assert(std::ranges::random_access_range<T>);
        static_assert(std::ranges::sized_range<T>);
        static_assert(std::ranges::borrowed_range<T>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& g = c.literals();
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.literals();

        auto it = g.begin();
        auto end = g.end();

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);

        BOOST_TEST((end - it) == 23);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.literals();

        auto [it, end] = g;

        BOOST_TEST((end - it) == 23);
    }

    BOOST_AUTO_TEST_CASE(borrowed_range_)
    {
        mime::cache c { mime_data_dir() };

        auto it = c.literals().begin();

        BOOST_TEST((*it).pattern() == ".gitattributes");
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.literals();

        BOOST_TEST(g.size() == 23);
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.literals();

        auto const& ent = g.front();

        BOOST_TEST(ent.pattern() == ".gitattributes");
        BOOST_TEST(ent.mime_type() == "application/x-gitattributes");
        BOOST_TEST(ent.flags() == 0);
        BOOST_TEST(ent.weight() == 50);
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.literals();

        auto const& ent = g.back();

        BOOST_TEST(ent.pattern() == "winmail.dat");
        BOOST_TEST(ent.mime_type() == "application/vnd.ms-tnef");
        BOOST_TEST(ent.flags() == 0);
        BOOST_TEST(ent.weight() == 50);
    }

    BOOST_AUTO_TEST_CASE(middle_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.literals();

        auto const& ent = g[g.size() / 2];

        BOOST_TEST(ent.pattern() == "gmon.out");
        BOOST_TEST(ent.mime_type() == "application/x-profile");
        BOOST_TEST(ent.flags() == 0);
        BOOST_TEST(ent.weight() == 50);
    }

BOOST_AUTO_TEST_SUITE_END() // literals_

} // namespace testing
