#include "src/magic.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include "src/cache.hpp"
#include "src/directory.hpp"
#include "src/magic.hpp"

#include <concepts>
#include <iterator>
#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/array.hpp>
#include <stream9/ranges/at.hpp>
#include <stream9/ranges/back.hpp>
#include <stream9/ranges/empty.hpp>
#include <stream9/ranges/front.hpp>

namespace testing {

using namespace std::literals::string_view_literals;

using stream9::array;

BOOST_AUTO_TEST_SUITE(magics_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::magics;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
        static_assert(std::ranges::random_access_range<T>);
        static_assert(std::ranges::sized_range<T>);
        static_assert(std::ranges::borrowed_range<T>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& g = c.magics();
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.magics();

        auto it = g.begin();
        auto end = g.end();

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.magics();

        auto [it, end] = g;

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);
    }

    BOOST_AUTO_TEST_CASE(borrowed_range_)
    {
        mime::cache c { mime_data_dir() };

        auto it = c.magics().begin();

        BOOST_TEST((*it).mime_type() == "application/vnd.stardivision.writer");
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.magics();

        BOOST_TEST(g.size() == 464);
    }

    BOOST_AUTO_TEST_CASE(max_extent_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.magics();

        BOOST_TEST(g.max_extent() == 18730);
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.magics();

        auto const& ent = rng::front(g);

        BOOST_TEST(ent.priority() == 90);
        BOOST_TEST(ent.mime_type() == "application/vnd.stardivision.writer");
        BOOST_TEST(ent.size() == 1);

        auto const& match = rng::front(ent);
        BOOST_TEST(match.range_start() == 2089);
        BOOST_TEST(match.range_length() == 1);
        BOOST_TEST(match.word_size() == 1);
        BOOST_TEST(std::ranges::equal(match.value(), "StarWriter"sv));
        BOOST_TEST(!match.mask());
        BOOST_TEST(match.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(traverse_)
    {
#if 0
        mime::cache c { mime_data_dir() };
        auto const& g = c.magics();

        int i = 0;
        for (auto const& ent: g) {
            if (ent.mime_type() == "application/epub+zip"sv) {
                std::cout << i << ": " << ent.mime_type() << ":" << ent.size() << "\n";
            }

            ++i;
        }
#endif
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.magics();

        auto const& ent = rng::back(g);

        BOOST_TEST(ent.priority() == 10);
        BOOST_TEST(ent.mime_type() == "text/x-tex");
        BOOST_TEST(ent.size() == 1);

        auto const& match = rng::front(ent);
        BOOST_TEST(match.range_start() == 0);
        BOOST_TEST(match.range_length() == 1);
        BOOST_TEST(match.word_size() == 1);
        BOOST_TEST(std::ranges::equal(match.value(), "%"sv));
        BOOST_TEST(!match.mask());
        BOOST_TEST(match.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(middle_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.magics();

        auto const& ent = g[g.size() / 2];

        BOOST_TEST(ent.priority() == 50);
        BOOST_TEST(ent.mime_type() == "application/x-ips-patch");
        BOOST_TEST(ent.size() == 1);

        auto const& match = ent.front();
        BOOST_TEST(match.range_start() == 0);
        BOOST_TEST(match.range_length() == 1);
        BOOST_TEST(match.word_size() == 1);
        BOOST_TEST(std::ranges::equal(match.value(), "PATCH"sv));
        BOOST_TEST(!match.mask());
        BOOST_TEST(match.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(match_type_little32_)
    {
        mime::cache c { mime_data_dir() };
        auto const& ent = c.magics()[87];

        BOOST_TEST(ent.priority() == 60);
        BOOST_TEST(ent.mime_type() == "application/x-arc");
        BOOST_TEST(ent.size() == 6);

        auto const& first = rng::front(ent);
        BOOST_TEST(first.range_start() == 0);
        BOOST_TEST(first.range_length() == 1);
        BOOST_TEST(first.word_size() == 1);

        auto eq = [](char const a, unsigned char const b) {
            return static_cast<unsigned char>(a) == b;
        };

        boost::endian::little_uint32_t value { 0x0000081a };
        static_assert(sizeof(value) == 4);
        BOOST_TEST(std::ranges::equal(first.value(), std::span(value.data(), 4)));
        boost::endian::little_uint32_t mask { 0x8080ffff };
        static_assert(sizeof(mask) == 4);
        BOOST_REQUIRE(first.mask());
        BOOST_TEST(std::ranges::equal(*first.mask(), std::span(mask.data(), 4), eq));

        BOOST_TEST(first.size() == 0);

        auto const& last = rng::back(ent);
        BOOST_TEST(last.range_start() == 0);
        BOOST_TEST(last.range_length() == 1);
        BOOST_TEST(last.word_size() == 1);

        boost::endian::little_uint32_t last_value { 0x0000061a };
        static_assert(sizeof(last_value) == 4);
        BOOST_TEST(std::ranges::equal(last.value(), std::span(last_value.data(), 4)));
        boost::endian::little_uint32_t last_mask { 0x8080ffff };
        static_assert(sizeof(last_mask) == 4);
        BOOST_REQUIRE(last.mask());
        BOOST_TEST(std::ranges::equal(*last.mask(), std::span(last_mask.data(), 4), eq));

        BOOST_TEST(last.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(nested_match_)
    {
        mime::cache c { mime_data_dir() };
        auto const& ent = c.magics()[37];

        BOOST_TEST(ent.priority() == 70);
        BOOST_TEST(ent.mime_type() == "application/epub+zip");
        BOOST_TEST(ent.size() == 1);

        auto const& m1 = rng::front(ent);
        BOOST_TEST(m1.range_start() == 0);
        BOOST_TEST(m1.range_length() == 1);
        BOOST_TEST(m1.word_size() == 1);
        BOOST_TEST(std::ranges::equal(m1.value(), "PK\x03\x04"sv));
        BOOST_TEST(!m1.mask());
        BOOST_TEST(m1.size() == 1);

        auto const& m2 = rng::front(m1);
        BOOST_TEST(m2.range_start() == 30);
        BOOST_TEST(m2.range_length() == 1);
        BOOST_TEST(m2.word_size() == 1);
        BOOST_TEST(std::ranges::equal(m2.value(), "mimetype"sv));
        BOOST_TEST(!m2.mask());
        BOOST_TEST(m2.size() == 2);

        auto const& m3 = rng::at(m2, 0);
        BOOST_TEST(m3.range_start() == 38);
        BOOST_TEST(m3.range_length() == 1);
        BOOST_TEST(m3.word_size() == 1);
        BOOST_TEST(std::ranges::equal(m3.value(), "application/epub+zip"sv));
        BOOST_TEST(!m3.mask());
        BOOST_TEST(rng::empty(m3));

        auto const& m4 = rng::at(m2, 1);
        BOOST_TEST(m4.range_start() == 43);
        BOOST_TEST(m4.range_length() == 1);
        BOOST_TEST(m4.word_size() == 1);
        BOOST_TEST(std::ranges::equal(m4.value(), "application/epub+zip"sv));
        BOOST_TEST(!m4.mask());
        BOOST_TEST(rng::empty(m4));
    }

    BOOST_AUTO_TEST_CASE(search_1_)
    {
        // target: match with children
        // <magic priority="70">
        //   <match type="string" value="PK\003\004" offset="0">
        //     <match type="string" value="mimetype" offset="30">
        //       <match type="string" value="application/epub+zip" offset="38"/>
        //       <match type="string" value="application/epub+zip" offset="43"/>
        //     </match>
        //   </match>
        // </magic>
        mime::cache c { mime_data_dir() };
        auto const& magics = c.magics();

        char data[256] = {};
        std::strcpy(&data[0], "PK\003\004");
        std::strcpy(&data[30], "mimetype");
        std::strcpy(&data[43], "application/epub+zip");

        array<mime::magic> result;
        magics.search(result, data);

        BOOST_REQUIRE(result.size() == 2);
        BOOST_TEST(result[0].mime_type() == "application/epub+zip");
        BOOST_TEST(result[0].priority() == 70);
        BOOST_TEST(result[1].mime_type() == "application/zip");
        BOOST_TEST(result[1].priority() == 60);
    }

    BOOST_AUTO_TEST_CASE(search_2_)
    {
        // target: match with mask
        // <magic priority="80">
        //   <match type="string" value="CDRXvrsn" mask="0xffffff00ffffffff" offset="8"/>
        // </magic>

        mime::cache c { mime_data_dir() };
        auto const& magics = c.magics();

        char data[256] = {};
        std::strcpy(&data[8], "CDR2vrsn");

        array<mime::magic> result;
        magics.search(result, data);

        BOOST_REQUIRE(result.size() == 1);
        BOOST_TEST(result[0].mime_type() == "application/vnd.corel-draw");
        BOOST_TEST(result[0].priority() == 80);
    }

BOOST_AUTO_TEST_SUITE_END() // magics_

} // namespace testing
