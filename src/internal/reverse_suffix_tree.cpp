#include "src/reverse_suffix_tree.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include "src/cache.hpp"
#include "src/directory.hpp"
#include "src/glob_record.hpp"

#include <concepts>
#include <iterator>
#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/array.hpp>
#include <stream9/ranges/back.hpp>
#include <stream9/ranges/front.hpp>

namespace testing {

using namespace std::literals;

using stream9::array;

BOOST_AUTO_TEST_SUITE(reverse_suffix_tree_)

    auto count_nodes(mime::suffix_tree const& t)
    {
        struct counter {
            void operator()(mime::tree_branch_node const& n)
            {
                ++num_branch;

                for (auto const& c: n) {
                    std::visit(*this, c);
                }
            }

            void operator()(mime::tree_leaf_node const&)
            {
                ++num_leaf;
            }

            int64_t num_branch = 0;
            int64_t num_leaf = 0;
        };

        counter v;
        for (auto const& n: t) {
            std::visit(v, n);
        }

        return v;
    }

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::suffix_tree;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
        static_assert(std::ranges::random_access_range<T>);
        static_assert(std::ranges::sized_range<T>);
        static_assert(std::ranges::borrowed_range<T>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& g = c.reverse_suffix_tree();
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.reverse_suffix_tree();

        auto it = g.begin();
        auto end = g.end();

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.reverse_suffix_tree();

        auto [it, end] = g;

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);
    }

    BOOST_AUTO_TEST_CASE(borrowed_range_)
    {
        mime::cache c { mime_data_dir() };

        auto it = c.reverse_suffix_tree().begin();

        auto const n = (*it);
        BOOST_TEST(std::holds_alternative<mime::tree_branch_node>(n));
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.reverse_suffix_tree();

        BOOST_TEST(g.size() == 40);
    }

    BOOST_AUTO_TEST_CASE(count_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.reverse_suffix_tree();

        auto const[ branch, leaf ] = count_nodes(g);
        BOOST_TEST(branch == 3061);
        BOOST_TEST(leaf == 1142);
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.reverse_suffix_tree();

        auto const& ent = rng::front(g);

        auto* const n = std::get_if<mime::tree_branch_node>(&ent);
        BOOST_REQUIRE(n);
        BOOST_TEST(n->character() == '%');
        BOOST_TEST(n->size() == 1);
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.reverse_suffix_tree();

        auto const& ent = rng::back(g);

        auto* const n = std::get_if<mime::tree_branch_node>(&ent);
        BOOST_REQUIRE(n);
        BOOST_TEST(n->character() == '~');
        BOOST_TEST(n->size() == 1);
    }

    BOOST_AUTO_TEST_CASE(middle_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.reverse_suffix_tree();

        auto const& ent = g[g.size() / 2];

        auto* const n = std::get_if<mime::tree_branch_node>(&ent);
        BOOST_REQUIRE(n);
        BOOST_TEST(n->character() == 'h');
        BOOST_TEST(n->size() == 9);
    }

    BOOST_AUTO_TEST_CASE(leaf_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.reverse_suffix_tree();

        auto const& n1 = g[12];
        BOOST_REQUIRE(std::holds_alternative<mime::tree_branch_node>(n1));
        auto const& b1 = std::get<mime::tree_branch_node>(n1);
        BOOST_REQUIRE(b1.character() == 'C');
        BOOST_TEST(b1.size() == 1);

        auto const n2 = rng::front(b1);
        BOOST_REQUIRE(std::holds_alternative<mime::tree_branch_node>(n2));
        auto const& b2 = std::get<mime::tree_branch_node>(n2);
        BOOST_REQUIRE(b2.character() == '.');
        BOOST_TEST(b2.size() == 1);

        auto const n3 = rng::front(b2);
        BOOST_REQUIRE(std::holds_alternative<mime::tree_leaf_node>(n3));
        auto const& l1 = std::get<mime::tree_leaf_node>(n3);

        BOOST_TEST(l1.mime_type() == "text/x-c++src");
        BOOST_TEST(l1.flags() == 1);
        BOOST_TEST(l1.weight() == 50);
    }

    BOOST_AUTO_TEST_CASE(search_)
    {
        mime::cache c { mime_data_dir() };
        auto const& tree = c.reverse_suffix_tree();

        array<mime::glob_record> matches;
        tree.search(matches, "test.cpp");

        BOOST_REQUIRE(matches.size() == 1);
        BOOST_TEST(matches[0].pattern == "*.cpp");
        BOOST_TEST(matches[0].mime_type == "text/x-c++src"sv);
        BOOST_TEST(matches[0].flags == 0);
        BOOST_TEST(matches[0].weight == 50);

        matches.clear();
        tree.search(matches, "TEST.CPP");

        BOOST_REQUIRE(matches.size() == 1);
        BOOST_TEST(matches[0].pattern == "*.cpp");
        BOOST_TEST(matches[0].mime_type == "text/x-c++src"sv);
        BOOST_TEST(matches[0].flags == 0);
        BOOST_TEST(matches[0].weight == 50);
    }

BOOST_AUTO_TEST_SUITE_END() // reverse_suffix_tree_

} // namespace testing
