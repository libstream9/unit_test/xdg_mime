#include "src/subclasses.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include "src/cache.hpp"

#include <iterator>
#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/ranges/back.hpp>
#include <stream9/ranges/front.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(subclass_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::subclass;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto s = rng::front(c.subclasses());

        [[maybe_unused]] auto [mime_type, parents] = s;
    }

BOOST_AUTO_TEST_SUITE_END() // subclass_

BOOST_AUTO_TEST_SUITE(subclasses_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::subclasses;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
        static_assert(std::ranges::random_access_range<T>);
        static_assert(std::ranges::sized_range<T>);
        static_assert(std::ranges::borrowed_range<T>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& s = c.subclasses();
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.subclasses();

        auto it = s.begin();
        auto end = s.end();

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);

        BOOST_TEST((end - it) == 453);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.subclasses();

        auto [it, end] = s;

        BOOST_TEST((end - it) == 453);
    }

    BOOST_AUTO_TEST_CASE(borrowed_range_)
    {
        mime::cache c { mime_data_dir() };

        auto it = c.subclasses().begin();

        BOOST_TEST((*it).mime_type() == "application/atom+xml");
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.subclasses();

        BOOST_TEST(s.size() == 453);
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.subclasses();

        auto const& ent = rng::front(s);

        BOOST_TEST(ent.mime_type() == "application/atom+xml");
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.subclasses();

        auto const& ent = rng::back(s);

        BOOST_TEST(ent.mime_type() == "x-content/win32-software");
    }

    BOOST_AUTO_TEST_CASE(middle_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.subclasses();

        auto const& ent = s[s.size() / 2];

        BOOST_TEST(ent.mime_type() == "application/x-xzpdf");
    }

    BOOST_AUTO_TEST_CASE(search_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.subclasses();

        auto const& p = s.search("application/x-xzpdf");
        BOOST_REQUIRE(p.size() == 1);
        BOOST_TEST(rng::front(p) == "application/x-xz");
    }

BOOST_AUTO_TEST_SUITE_END() // subclasses_

BOOST_AUTO_TEST_SUITE(parents_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::parents;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
        static_assert(std::ranges::random_access_range<T>);
        static_assert(std::ranges::sized_range<T>);
        static_assert(std::ranges::borrowed_range<T>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        auto const& s = c.subclasses();

        [[maybe_unused]] auto p = rng::front(s).parents();
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        mime::cache c { mime_data_dir() };
        auto p = rng::front(c.subclasses()).parents();

        auto it = p.begin();
        auto end = p.end();

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);

        BOOST_TEST((end - it) == 1);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto p = rng::front(c.subclasses()).parents();

        auto [it, end] = p;

        BOOST_TEST((end - it) == 1);
    }

    BOOST_AUTO_TEST_CASE(borrowed_range_)
    {
        mime::cache c { mime_data_dir() };
        auto it = rng::front(c.subclasses()).parents().begin();

        BOOST_TEST((*it) == "application/xml");
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        mime::cache c { mime_data_dir() };
        auto p = rng::front(c.subclasses()).parents();

        BOOST_TEST(p.size() == 1);
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        mime::cache c { mime_data_dir() };
        auto p = rng::front(c.subclasses()).parents();

        auto const& ent = rng::front(p);

        BOOST_TEST(ent == "application/xml");
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        mime::cache c { mime_data_dir() };
        auto p = rng::front(c.subclasses()).parents();

        auto const& ent = rng::back(p);

        BOOST_TEST(ent == "application/xml");
    }

    BOOST_AUTO_TEST_CASE(middle_)
    {
        //TODO test it with artificial test data
    }

BOOST_AUTO_TEST_SUITE_END() // parents_

} // namespace testing
