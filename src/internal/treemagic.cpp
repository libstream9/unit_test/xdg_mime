#include "src/treemagic.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include "src/directory.hpp"
#include "src/treemagic_match.hpp"

#include <stream9/xdg/mime/database.hpp>

#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/array.hpp>
#include <stream9/path/concat.hpp> // operator/

namespace testing {

using stream9::array;
using stream9::path::operator/;

BOOST_AUTO_TEST_SUITE(treemagics_)

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::treemagics db { mime_data_dir() };
    }

    BOOST_AUTO_TEST_CASE(step2_)
    {
        mime::treemagics db { mime_data_dir() };

        struct data_t {
            char const* mime_type;
            int priority;
        } expected[] = {
            { "x-content/audio-dvd", 50 },
            { "x-content/ebook-reader", 50 },
            { "x-content/image-dcf", 50 },
            { "x-content/image-picturecd", 50 },
            { "x-content/ostree-repository", 50 },
            { "x-content/unix-software", 50 },
            { "x-content/video-bluray", 50 },
            { "x-content/video-dvd", 50 },
            { "x-content/video-hddvd", 50 },
            { "x-content/video-svcd", 50 },
            { "x-content/video-vcd", 50 },
            { "x-content/win32-software", 50 },
        };

        BOOST_REQUIRE(rng::size(db) == sizeof(expected) / sizeof(data_t));

        size_t i = 0;
        for (auto const& magic: db) {
            BOOST_TEST(magic.mime_type() == expected[i].mime_type);
            BOOST_TEST(magic.priority() == expected[i].priority);
            ++i;
        }
    }

    BOOST_AUTO_TEST_CASE(treemagic_1_)
    {
        mime::treemagics db { mime_data_dir() };

        auto const& magic = *db.begin();

        BOOST_TEST(magic.mime_type() == "x-content/audio-dvd");
        BOOST_TEST(magic.priority() == 50);

        BOOST_TEST(rng::size(magic) == 2);
        {
            auto const& match = magic.begin()[0];
            BOOST_TEST(match.path() == "AUDIO_TS/AUDIO_TS.IFO");
            BOOST_CHECK(match.type() == mime::treematch::type::file);
            BOOST_CHECK(!match.match_case());
            BOOST_CHECK(!match.executable());
            BOOST_CHECK(!match.non_empty());
        }
        {
            auto const& match = magic.begin()[1];
            BOOST_TEST(match.path() == "AUDIO_TS/AUDIO_TS.IFO;1");
            BOOST_CHECK(match.type() == mime::treematch::type::file);
            BOOST_CHECK(!match.match_case());
            BOOST_CHECK(!match.executable());
            BOOST_CHECK(!match.non_empty());
        }
    }

    BOOST_AUTO_TEST_CASE(treemagic_2_)
    {
        mime::treemagics db { mime_data_dir() };

        auto const& magic = db.begin()[4];

        BOOST_TEST(magic.mime_type() == "x-content/ostree-repository");
        BOOST_TEST(magic.priority() == 50);

        BOOST_REQUIRE(rng::size(magic) == 3);
        {
            auto const& match = magic.begin()[0];
            BOOST_TEST(match.path() == ".ostree");
            BOOST_CHECK(match.type() == mime::treematch::type::directory);
            BOOST_CHECK(match.match_case());
            BOOST_CHECK(!match.executable());
            BOOST_CHECK(match.non_empty());
        }
        {
            auto const& match = magic.begin()[1];
            BOOST_TEST(match.path() == "ostree/repo");
            BOOST_CHECK(match.type() == mime::treematch::type::directory);
            BOOST_CHECK(match.match_case());
            BOOST_CHECK(!match.executable());
            BOOST_CHECK(match.non_empty());
        }
        {
            auto const& match = magic.begin()[2];
            BOOST_TEST(match.path() == "var/lib/flatpak/repo");
            BOOST_CHECK(match.type() == mime::treematch::type::directory);
            BOOST_CHECK(match.match_case());
            BOOST_CHECK(!match.executable());
            BOOST_CHECK(match.non_empty());
        }
    }

    BOOST_AUTO_TEST_CASE(search_1_)
    {
        mime::treemagics magics { mime_data_dir() };
        mime::database db;

        array<mime::treemagic const*> result;

        magics.search(result, db, data_dir() / "mime_tree");

        BOOST_TEST_REQUIRE(result.size() == 5);
        BOOST_TEST(result[0]->mime_type() == "x-content/audio-dvd");
        BOOST_TEST(result[0]->priority() == 50);
        BOOST_TEST(result[1]->mime_type() == "x-content/image-dcf");
        BOOST_TEST(result[1]->priority() == 50);
        BOOST_TEST(result[2]->mime_type() == "x-content/unix-software");
        BOOST_TEST(result[2]->priority() == 50);
        BOOST_TEST(result[3]->mime_type() == "x-content/video-hddvd");
        BOOST_TEST(result[3]->priority() == 50);
        BOOST_TEST(result[4]->mime_type() == "x-content/win32-software");
        BOOST_TEST(result[4]->priority() == 50);
    }

BOOST_AUTO_TEST_SUITE_END() // treemagics_

} // namespace testing
