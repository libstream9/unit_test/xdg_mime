#include "src/xml_namespaces.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include "src/cache.hpp"

#include <concepts>
#include <iterator>
#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/ranges/back.hpp>
#include <stream9/ranges/front.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(xml_namespace_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::xml_namespace;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& ent = rng::front(c.xml_namespaces());
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& ent = rng::front(c.xml_namespaces());

        auto [namespace_uri, local_name, mime_type] = ent;

        static_assert(std::same_as<decltype(namespace_uri), st9::cstring_view>);
        static_assert(std::same_as<decltype(local_name), st9::cstring_view>);
        static_assert(std::same_as<decltype(mime_type), st9::cstring_view>);

        BOOST_TEST(namespace_uri == "http://schema.omg.org/spec/XMI/2.0");
        BOOST_TEST(local_name == "XMI");
        BOOST_TEST(mime_type == "text/x-xmi");
    }

BOOST_AUTO_TEST_SUITE_END() // xml_namespace_

BOOST_AUTO_TEST_SUITE(xml_namespaces_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = mime::xml_namespaces;

        static_assert(std::copy_constructible<T>);
        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
        static_assert(std::assignable_from<T&, T const&>);
        static_assert(std::assignable_from<T&, T&&>);
        static_assert(std::ranges::random_access_range<T>);
        static_assert(std::ranges::sized_range<T>);
        static_assert(std::ranges::borrowed_range<T>);
    }

    BOOST_AUTO_TEST_CASE(construct_)
    {
        mime::cache c { mime_data_dir() };
        [[maybe_unused]] auto const& g = c.xml_namespaces();
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.xml_namespaces();

        auto it = g.begin();
        auto end = g.end();

        static_assert(std::random_access_iterator<decltype(it)>);
        static_assert(std::sized_sentinel_for<decltype(end), decltype(it)>);

        BOOST_TEST((end - it) == 29);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.xml_namespaces();

        auto [it, end] = g;

        BOOST_TEST((end - it) == 29);
    }

    BOOST_AUTO_TEST_CASE(borrowed_range_)
    {
        mime::cache c { mime_data_dir() };

        auto it = c.xml_namespaces().begin();

        BOOST_TEST(
            (*it).namespace_uri() == "http://schema.omg.org/spec/XMI/2.0");
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.xml_namespaces();

        BOOST_TEST(g.size() == 29);
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.xml_namespaces();

        auto const& ent = rng::front(g);

        BOOST_TEST(ent.namespace_uri() == "http://schema.omg.org/spec/XMI/2.0");
        BOOST_TEST(ent.local_name() == "XMI");
        BOOST_TEST(ent.mime_type() == "text/x-xmi");
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.xml_namespaces();

        auto const& ent = rng::back(g);

        BOOST_TEST(ent.namespace_uri() == "urn:oasis:names:tc:xliff:document:1.1");
        BOOST_TEST(ent.local_name() == "xliff");
        BOOST_TEST(ent.mime_type() == "application/xliff+xml");
    }

    BOOST_AUTO_TEST_CASE(middle_)
    {
        mime::cache c { mime_data_dir() };
        auto const& g = c.xml_namespaces();

        auto const& ent = g[g.size() / 2];

        BOOST_TEST(ent.namespace_uri() == "http://www.w3.org/1998/Math/MathML");
        BOOST_TEST(ent.local_name() == "math");
        BOOST_TEST(ent.mime_type() == "application/mathml+xml");
    }

    BOOST_AUTO_TEST_CASE(search_)
    {
        mime::cache c { mime_data_dir() };
        auto const& db = c.xml_namespaces();

        auto const n = db.search("urn:ietf:params:xml:ns:metalink", "metalink");
        BOOST_REQUIRE(n);
        BOOST_TEST(*n == "application/metalink4+xml");
    }

BOOST_AUTO_TEST_SUITE_END() // xml_namespaces_

} // namespace testing
