#ifndef STREAM9_MIME_TEST_SRC_MAKE_PATHS_HPP
#define STREAM9_MIME_TEST_SRC_MAKE_PATHS_HPP

#include "namespace.hpp"

#include <filesystem>
#include <string>

namespace testing {

inline std::string
make_paths(std::string_view const& p)
{
    return std::string(p);
}

template<typename... Rest>
static std::string
make_paths(std::string_view const& p, Rest&&... rest)
{
    return std::string(p) + ":" + make_paths(std::forward<Rest>(rest)...);
}

} // namespace testing

#endif // STREAM9_MIME_TEST_SRC_MAKE_PATHS_HPP
