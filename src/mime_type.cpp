#include <stream9/xdg/mime/mime_type.hpp>

#include <stream9/xdg/mime/database.hpp>

#include "namespace.hpp"

#include "src/directory.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(mime_type_)

    BOOST_AUTO_TEST_CASE(comment_)
    {
        mime::database db;

        mime::mime_type m { db, "text/plain" };

        BOOST_TEST(m.comment() == "Plain text document");
        BOOST_TEST(m.comment("ja") == "平文文書"); // utf8
    }

    BOOST_AUTO_TEST_CASE(aliases_)
    {
        mime::database db;

        mime::mime_type m { db, "text/troff" };

        auto const& aliases = m.aliases();
        BOOST_REQUIRE(aliases.size() == 2);
        BOOST_TEST(aliases[0] == "application/x-troff");
        BOOST_TEST(aliases[1] == "text/x-troff");
    }

    BOOST_AUTO_TEST_CASE(base_classes_)
    {
        mime::database db;

        mime::mime_type m { db, "application/x-ruby" };

        auto const& bases = m.base_classes();
        BOOST_REQUIRE(bases.size() == 2);
        BOOST_CHECK(bases[0].name() == "application/x-executable");
        BOOST_CHECK(bases[1].name() == "text/plain");
    }

    BOOST_AUTO_TEST_CASE(icon_name_)
    {
        mime::database db;

        mime::mime_type m { db, "application/x-keepass2" };

        BOOST_TEST(m.icon_name() == "application-x-keepassx");
    }

    BOOST_AUTO_TEST_CASE(default_icon_name_)
    {
        mime::database db;

        mime::mime_type m { db, "text/plain" };

        BOOST_TEST(m.icon_name() == "text-plain");
    }

    BOOST_AUTO_TEST_CASE(generic_icon_name_)
    {
        mime::database db;

        mime::mime_type m { db, "application/x-xz" };

        BOOST_TEST(m.generic_icon_name() == "package-x-generic");
    }

    BOOST_AUTO_TEST_CASE(default_generic_icon_name_)
    {
        mime::database db;

        mime::mime_type m { db, "text/plain" };

        BOOST_TEST(m.generic_icon_name() == "text-x-generic");
    }

    BOOST_AUTO_TEST_CASE(acronym_)
    {
        mime::database db;

        mime::mime_type m { db, "text/csv" };

        BOOST_TEST(m.acronym() == "CSV");
    }

    BOOST_AUTO_TEST_CASE(expanded_acronym_)
    {
        mime::database db;

        mime::mime_type m { db, "text/csv" };

        BOOST_TEST(m.expanded_acronym() == "Comma Separated Values");
    }

    BOOST_AUTO_TEST_CASE(glob_patterns_)
    {
        mime::database db;

        mime::mime_type m { db, "text/troff" };

        auto const& globs = m.glob_patterns();

        BOOST_REQUIRE(globs.size() == 3);
        BOOST_TEST(globs[0] == "*.tr");
        BOOST_TEST(globs[1] == "*.roff");
        BOOST_TEST(globs[2] == "*.t");
    }

BOOST_AUTO_TEST_SUITE_END() // mime_type_

} // namespace testing
