#include "src/xml/mime_type.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/path/concat.hpp>

namespace testing {

namespace xml = stream9::xdg::mime::xml;

using stream9::path::operator/;

BOOST_AUTO_TEST_SUITE(xml_)
BOOST_AUTO_TEST_SUITE(mime_type_)

    BOOST_AUTO_TEST_CASE(construct_from_path_)
    {
        xml::mime_type mt { mime_data_dir() / "text/plain.xml" };

        BOOST_TEST(mt.name() == "text/plain");
        BOOST_TEST(mt.comment("eu") == "testu soileko dokumentua");
    }

    BOOST_AUTO_TEST_CASE(construct_from_name_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        BOOST_TEST(mt.name() == "text/plain");
        BOOST_TEST(mt.comment("") == "plain text");
    }

    BOOST_AUTO_TEST_CASE(comment_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        mt.set_comment("bar", "");
        mt.set_comment("foo", "ja");
        BOOST_TEST(mt.comment("ja") == "foo");
        BOOST_TEST(mt.comment("") == "bar");
        BOOST_TEST(mt.comment("foo") == "");
    }

    BOOST_AUTO_TEST_CASE(aliases_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        mt.append_alias("text/foo");
        mt.append_alias("text/bar");

        auto const& aliases = mt.aliases();
        BOOST_REQUIRE(aliases.size() == 2);
        BOOST_TEST(aliases[0] == "text/foo");
        BOOST_TEST(aliases[1] == "text/bar");
    }

    BOOST_AUTO_TEST_CASE(base_class_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        mt.append_base_class("text/foo");
        mt.append_base_class("text/bar");

        auto const& properties = mt.base_classes();
        BOOST_REQUIRE(properties.size() == 2);
        BOOST_TEST(properties[0] == "text/foo");
        BOOST_TEST(properties[1] == "text/bar");
    }

    BOOST_AUTO_TEST_CASE(icon_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        BOOST_TEST(mt.icon() == "");

        mt.set_icon("foo");
        BOOST_TEST(mt.icon() == "foo");

        mt.set_icon("bar");
        BOOST_TEST(mt.icon() == "bar");
    }

    BOOST_AUTO_TEST_CASE(generic_icon_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        BOOST_TEST(mt.generic_icon() == "");

        mt.set_generic_icon("foo");
        BOOST_TEST(mt.generic_icon() == "foo");

        mt.set_generic_icon("bar");
        BOOST_TEST(mt.generic_icon() == "bar");
    }

    BOOST_AUTO_TEST_CASE(acronym_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        mt.set_acronym("bar", "");
        mt.set_acronym("foo", "ja");
        BOOST_TEST(mt.acronym("ja") == "foo");
        BOOST_TEST(mt.acronym("") == "bar");
        BOOST_TEST(mt.acronym("foo") == "");
    }

    BOOST_AUTO_TEST_CASE(expanded_acronym_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        mt.set_expanded_acronym("bar", "");
        mt.set_expanded_acronym("foo", "ja");
        BOOST_TEST(mt.expanded_acronym("ja") == "foo");
        BOOST_TEST(mt.expanded_acronym("") == "bar");
        BOOST_TEST(mt.expanded_acronym("foo") == "");
    }

    BOOST_AUTO_TEST_CASE(glob_)
    {
        xml::mime_type mt { "text/plain", "plain text" };
        using glob = xml::mime_type::glob;

        auto globs = mt.globs();
        BOOST_REQUIRE(globs.empty());

        mt.append_glob("*.txt");
        mt.append_glob("*.doc", 40);
        globs = mt.globs();

        BOOST_REQUIRE(globs.size() == 2);
        BOOST_CHECK(globs[0] == glob("*.txt", 50));
        BOOST_CHECK(globs[1] == glob("*.doc", 40));
    }

    BOOST_AUTO_TEST_CASE(magic_)
    {
        xml::mime_type mt { "text/plain", "plain text" };
        using magic = xml::mime_type::magic;
        using match = xml::mime_type::match;

        auto magics = mt.magics();
        BOOST_REQUIRE(magics.empty());

        mt.append_magic(magic {
            50, {
                match { "type", "offset", "value", "mask", {} },
            }
        });

        magics = mt.magics();
        BOOST_REQUIRE(magics.size() == 1);
        BOOST_TEST(magics[0].priority == 50);
        BOOST_REQUIRE(magics[0].matches.size() == 1);
        BOOST_TEST(magics[0].matches[0].type == "type");
        BOOST_TEST(magics[0].matches[0].offset == "offset");
        BOOST_TEST(magics[0].matches[0].value == "value");
        BOOST_TEST(magics[0].matches[0].mask == "mask");
        BOOST_TEST(magics[0].matches[0].children.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(treemagic_)
    {
        xml::mime_type mt { "text/plain", "plain text" };
        using treemagic = xml::mime_type::treemagic;
        using treematch = xml::mime_type::treematch;

        auto magics = mt.treemagics();
        BOOST_REQUIRE(magics.empty());

        mt.append_treemagic(treemagic {
            40, {
                treematch {
                    "path", "type", "match-case", "executable",
                    "non-empty", "mimetype", {}
                },
            }
        });

        magics = mt.treemagics();
        BOOST_REQUIRE(magics.size() == 1);
        BOOST_TEST(magics[0].priority == 40);
        BOOST_REQUIRE(magics[0].matches.size() == 1);
        BOOST_TEST(magics[0].matches[0].path == "path");
        BOOST_TEST(magics[0].matches[0].type == "type");
        BOOST_TEST(magics[0].matches[0].match_case == "match-case");
        BOOST_TEST(magics[0].matches[0].executable == "executable");
        BOOST_TEST(magics[0].matches[0].non_empty == "non-empty");
        BOOST_TEST(magics[0].matches[0].mime_type == "mimetype");
        BOOST_TEST(magics[0].matches[0].children.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(root_xml_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        auto root_xmls = mt.root_xmls();
        BOOST_REQUIRE(root_xmls.empty());

        mt.append_root_xml("namespace1", "name1");
        mt.append_root_xml("namespace2", "name2");

        root_xmls = mt.root_xmls();
        BOOST_REQUIRE(root_xmls.size() == 2);
        BOOST_TEST(root_xmls[0].namespace_uri == "namespace1");
        BOOST_TEST(root_xmls[0].local_name == "name1");
        BOOST_TEST(root_xmls[1].namespace_uri == "namespace2");
        BOOST_TEST(root_xmls[1].local_name == "name2");
    }

    BOOST_AUTO_TEST_CASE(glob_deleteall_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        BOOST_REQUIRE(!mt.glob_deleteall());

        mt.set_glob_deleteall(true);
        BOOST_TEST(mt.glob_deleteall());

        mt.set_glob_deleteall(true);
        BOOST_TEST(mt.glob_deleteall());

        mt.set_glob_deleteall(false);
        BOOST_TEST(!mt.glob_deleteall());
    }

    BOOST_AUTO_TEST_CASE(magic_deleteall_)
    {
        xml::mime_type mt { "text/plain", "plain text" };

        BOOST_REQUIRE(!mt.magic_deleteall());

        mt.set_magic_deleteall(true);
        BOOST_TEST(mt.magic_deleteall());

        mt.set_magic_deleteall(true);
        BOOST_TEST(mt.magic_deleteall());

        mt.set_magic_deleteall(false);
        BOOST_TEST(!mt.magic_deleteall());
    }

    BOOST_AUTO_TEST_CASE(merge_comment_)
    {
        xml::mime_type mt1 { "text/plain", "plain text" };
        mt1.set_comment("xxx", "br");

        BOOST_TEST(mt1.comments().size() == 2);
        BOOST_TEST(mt1.comment("") == "plain text");
        BOOST_TEST(mt1.comment("br") == "xxx");
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            mt2.set_comment("foo", "ja");
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.comments().size() == 3);
        BOOST_TEST(mt1.comment("") == "Plain text");
        BOOST_TEST(mt1.comment("br") == "xxx");
        BOOST_TEST(mt1.comment("ja") == "foo");
    }

    BOOST_AUTO_TEST_CASE(merge_aliases_)
    {
        xml::mime_type mt1 { "text/plain", "plain text" };
        mt1.append_alias("foo");

        BOOST_TEST(mt1.aliases().size() == 1);
        BOOST_TEST(mt1.aliases()[0] == "foo");
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            mt2.append_alias("foo");
            mt2.append_alias("bar");
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.aliases().size() == 2);
        BOOST_TEST(mt1.aliases()[0] == "foo");
        BOOST_TEST(mt1.aliases()[1] == "bar");
    }

    BOOST_AUTO_TEST_CASE(merge_base_classes_)
    {
        xml::mime_type mt1 { "text/plain", "plain text" };
        mt1.append_base_class("base1");

        BOOST_TEST(mt1.base_classes().size() == 1);
        BOOST_TEST(mt1.base_classes()[0] == "base1");
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            mt2.append_base_class("base1");
            mt2.append_base_class("base2");
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.base_classes().size() == 2);
        BOOST_TEST(mt1.base_classes()[0] == "base1");
        BOOST_TEST(mt1.base_classes()[1] == "base2");
    }

    BOOST_AUTO_TEST_CASE(merge_icon_)
    {
        xml::mime_type mt1 { "text/plain", "plain text" };
        mt1.set_icon("icon1");

        BOOST_TEST(mt1.icon() == "icon1");
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            mt2.set_icon("icon2");
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.icon() == "icon2");
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            BOOST_TEST(mt2.icon().empty());
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.icon() == "icon2");
    }

    BOOST_AUTO_TEST_CASE(merge_generic_icon_)
    {
        xml::mime_type mt1 { "text/plain", "plain text" };
        mt1.set_generic_icon("generic_icon1");

        BOOST_TEST(mt1.generic_icon() == "generic_icon1");
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            mt2.set_generic_icon("generic_icon2");
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.generic_icon() == "generic_icon2");
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            BOOST_TEST(mt2.generic_icon().empty());
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.generic_icon() == "generic_icon2");
    }

    BOOST_AUTO_TEST_CASE(merge_acronym_)
    {
        xml::mime_type mt1 { "text/plain", "plain text" };
        mt1.set_acronym("acronym1", "");
        mt1.set_acronym("acronym2", "br");

        BOOST_TEST(mt1.acronyms().size() == 2);
        BOOST_TEST(mt1.acronym("") == "acronym1");
        BOOST_TEST(mt1.acronym("br") == "acronym2");
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            mt2.set_acronym("acronym3", "ja");
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.acronyms().size() == 3);
        BOOST_TEST(mt1.acronym("") == "acronym1");
        BOOST_TEST(mt1.acronym("br") == "acronym2");
        BOOST_TEST(mt1.acronym("ja") == "acronym3");
    }

    BOOST_AUTO_TEST_CASE(merge_expanded_acronym_)
    {
        xml::mime_type mt1 { "text/plain", "plain text" };
        mt1.set_expanded_acronym("expanded_acronym1", "");
        mt1.set_expanded_acronym("expanded_acronym2", "br");

        BOOST_TEST(mt1.expanded_acronyms().size() == 2);
        BOOST_TEST(mt1.expanded_acronym("") == "expanded_acronym1");
        BOOST_TEST(mt1.expanded_acronym("br") == "expanded_acronym2");
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            mt2.set_expanded_acronym("expanded_acronym3", "ja");
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.expanded_acronyms().size() == 3);
        BOOST_TEST(mt1.expanded_acronym("") == "expanded_acronym1");
        BOOST_TEST(mt1.expanded_acronym("br") == "expanded_acronym2");
        BOOST_TEST(mt1.expanded_acronym("ja") == "expanded_acronym3");
    }

    BOOST_AUTO_TEST_CASE(merge_glob_)
    {
        xml::mime_type mt1 { "text/plain", "plain text" };
        mt1.append_glob("glob1");

        BOOST_TEST(mt1.globs().size() == 1);
        BOOST_TEST(mt1.globs()[0].pattern == "glob1");
        BOOST_TEST(mt1.globs()[0].weight == 50);
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            mt2.append_glob("glob1", 40);
            mt2.append_glob("glob2", 60);
            BOOST_TEST(!mt2.glob_deleteall());
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.globs().size() == 2);
        BOOST_TEST(mt1.globs()[0].pattern == "glob1");
        BOOST_TEST(mt1.globs()[0].weight == 40);
        BOOST_TEST(mt1.globs()[1].pattern == "glob2");
        BOOST_TEST(mt1.globs()[1].weight == 60);
        {
            xml::mime_type mt2 { "text/plain", "Plain text" };
            mt2.append_glob("glob3");
            mt2.set_glob_deleteall();
            mt1.merge(mt2);
        }
        BOOST_TEST(mt1.globs().size() == 1);
        BOOST_TEST(mt1.globs()[0].pattern == "glob3");
        BOOST_TEST(mt1.globs()[0].weight == 50);
    }

BOOST_AUTO_TEST_SUITE_END() // mime_type_
BOOST_AUTO_TEST_SUITE_END() // xml_

} // namespace testing
